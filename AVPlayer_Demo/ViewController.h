//
//  ViewController.h
//  AVPlayer_Demo
//
//  Created by CraterZone on 29/08/15.
//  Copyright (c) 2015 CraterZone. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AudioUnit/AudioUnit.h>
#import  <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioFile.h>
#import <Availability.h>



@interface ViewController : UIViewController
{
    AVPlayer *player;
}



//@property (weak, nonatomic) IBOutlet UIImageView *playImage;
@property (weak, nonatomic) IBOutlet UISlider *audioSliderBar;

@property (weak, nonatomic) IBOutlet UILabel *audioCurrentTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *audioButton;
@property  BOOL audioPlayerWillStop;
@property BOOL scrubbing;


@end

