//
//  ViewController.m
//  AVPlayer_Demo
//
//  Created by CraterZone on 29/08/15.
//  Copyright (c) 2015 CraterZone. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end



@implementation ViewController
{
    
    AVPlayerItem *playerItem;
    BOOL isAlredayPlaying;
    float seconds;
    BOOL  isCurrentlyPlaying;
    NSTimer *nsTimer;
    float duration_seconds;
    NSTimeInterval duration;
    NSTimeInterval currentTime;
    BOOL test;
    NSTimer *nsTimer2;
}

@synthesize audioButton,audioPlayerWillStop;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSBundle mainBundle] loadNibNamed:@"ViewController" owner:self options:nil];

    
    
    
    test=NO;
    isAlredayPlaying=NO;
    isCurrentlyPlaying =YES;
    audioPlayerWillStop=NO;
    
    // Set audio slider bar color
    
    [_audioSliderBar setThumbImage:[_audioSliderBar thumbImageForState:UIControlStateNormal] forState:UIControlStateNormal];
    _audioSliderBar.minimumTrackTintColor =[UIColor whiteColor];
    _audioSliderBar.maximumTrackTintColor = [UIColor colorWithRed:92.0/255.0 green:92.0/255.0  blue:92.0/255.0  alpha:1];
    
    //[self.AudioButton setImage:[UIImage imageNamed:@"demoPlayButton.png"] forState:UIControlStateNormal];
    [self.audioButton addTarget:self action:@selector(playButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    // Initialization code

    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)playButtonClicked
{
    
        // change play , puase button on click
        
        if (audioButton.selected)
        {
            audioButton.selected = NO;
            printf("\n\ndo audioButton.selected = NO; \n\n");
            
        }
        
        else
        {
            printf("\n\ndo audioButton.selected = YES; \n\n");
            audioButton.selected = YES;
        }
        
        if (!isAlredayPlaying) {
            printf("\n\nStarted song from 0\n");
            isAlredayPlaying=YES;
            
            
            //Another good audio file url :  http://www.robtowns.com/music/blind_willie.mp3
            
            //#define AUDIO_FILE_URL @"http://soundx.mp3slash.net/indian/jhankar_beats/1%28mp3pk.com%29.mp3"
            
            
            NSURL *url = [NSURL URLWithString:@"http://soundx.mp3slash.net/indian/jhankar_beats/1%28mp3pk.com%29.mp3"];
            
            // 2.46
            
            [self.audioSliderBar setMinimumValue:0.0];
            
            
            
            playerItem = [AVPlayerItem playerItemWithURL:url];
            
          
            
            player = [AVPlayer playerWithPlayerItem:playerItem];
            
            player.volume=5.0;
            
            
            
            
            
            
            nsTimer=[NSTimer scheduledTimerWithTimeInterval:0.001 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
            
            [player play];
            
            //  [self.contentView. setValue:currentTime animated:YES];
            
            
        }
        
        else
        {
            if (isCurrentlyPlaying)
                
            {
                [nsTimer invalidate];
                isCurrentlyPlaying=NO;
                [player pause];
                printf("\n\npause done\n");
                
            }
            else
            {
                nsTimer=[NSTimer scheduledTimerWithTimeInterval:0.001 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
                isCurrentlyPlaying=YES;
                [player play];
                printf("\n\nplay done\n");
            }
            
        }
          // internet connection check funtion, ended
    
    
    }


- (void)updateTime:(NSTimer *)timer {
    
    // [self.audioSliderBar setMaximumValue: 2.46];
    printf("\n\nIn updating slider bar time, function ");
    
    currentTime = CMTimeGetSeconds(playerItem.currentTime);
    
    duration = CMTimeGetSeconds(playerItem.duration);
    
    
    
    
    //self.audioSliderBar.continuous = YES;
    
    
    
    //    NSLog(@"= current time = %f",currentTime);
    //      NSLog(@"= duration time = %f",duration);
    
    //    if (currentTime==duration) {
    //        [self itemDidFinishPlaying];
    //
    //    }
    //
    
    //if (!test) {
    
    [self.audioSliderBar setValue:(currentTime/duration)];
    
    //}
    
    
    //    [progressbar setProgress:0.7];
    // [printf("\n current time = %f",(currentTime/duration));
    
    float minutes = floor(currentTime/60);
    seconds =currentTime - (minutes * 60);
    
    float duration_minutes = floor(duration/60);
    duration_seconds =
    duration - (duration_minutes * 60);
    
    //    NSString *timeInfoString = [[NSString alloc]
    //                                initWithFormat:@"%0.0f.%0.0f / %0.0f.%0.0f",
    //                                minutes, seconds,
    //                                duration_minutes, duration_seconds];
    //
    
    
    NSString *timeInfoString = [[NSString alloc]
                                initWithFormat:@"%0.0f:%0.0f",
                                minutes, seconds ];//],
    //duration_minutes, duration_seconds];
    
    if  (!(player.currentItem && player.rate != 0 )&& isCurrentlyPlaying)
    {
        if (audioPlayerWillStop) {
            
            isAlredayPlaying=NO;
            [nsTimer invalidate];
            
        }
        
        else {
            
            [player play];
            printf("\n\n\n Force to play song...");
            
        }
        
        
    }
    self.audioCurrentTimeLabel.text = timeInfoString;
    //[player play];
}

-(void)itemDidFinishPlaying {
    
    printf("\n\nGreat song finished");
    ///audioButton.selected = NO;
    
    //    UIImage *btnImage = [UIImage imageNamed:@"play.png"];
    //    [audioButton setImage:btnImage forState:UIControlStateSelected];
    
    
    if (audioButton.selected)
    {
        audioButton.selected = NO;
        printf("\n\ndo audioButton.selected = NO; \n\n");
        
    }
    
    else
    {
        printf("\n\ndo audioButton.selected = YES; \n\n");
        
        audioButton.selected = YES;
        
    }
    
    
    
    isAlredayPlaying=NO;
    [nsTimer invalidate];
    //return 0;
    
}



-(void)handleAudioPlayer:(NSNotification *)notification {
    
    audioButton.selected = NO;
    [player pause];
    [self.audioSliderBar setValue:0.0];
    self.audioCurrentTimeLabel.text=@"0:0";
    isAlredayPlaying=NO;
    [nsTimer invalidate];
}


- (IBAction)audioPlayerValueChanged:(UISlider *)aSlider  {
    
    // called 2nd
    
    printf("\n\nprogess bar touch, now value will change according to the bar value\n\n");
    
    
    
    
    [player seekToTime:CMTimeMakeWithSeconds(duration_seconds*aSlider.value, 1)];
    
    
    
    
    
    
    //[player setCurrentTime:aSlider.value];
    
}

- (void)updateTime2:(NSTimer *)timer {
    
    printf("\n\nIn NEW updating slider, function ");
    
    currentTime = CMTimeGetSeconds(playerItem.currentTime);
    duration = CMTimeGetSeconds(playerItem.duration);
    float minutes = floor(currentTime/60);
    seconds =currentTime - (minutes * 60);
    
    float duration_minutes = floor(duration/60);
    duration_seconds =
    duration - (duration_minutes * 60);
    
    
    [self.audioSliderBar setValue:(currentTime/duration)];
    
    
        NSString *timeInfoString = [[NSString alloc]
                                initWithFormat:@"%0.0f:%0.0f",
                                minutes, seconds ];
    //duration_minutes, duration_seconds];
    
    if  (!(player.currentItem && player.rate != 0 )&& isCurrentlyPlaying)
    {
        if (audioPlayerWillStop) {
            
            isAlredayPlaying=NO;
            //... [nsTimer invalidate];
            
        }
        
        else {
            
            [player play];
            printf("\n\n\n Force to play song...");
            
        }
        
        
    }
    self.audioCurrentTimeLabel.text = timeInfoString;
    //[player play];
}


- (IBAction)touchDown:(id)sender {
    
    
    // called 1st
    
    
    [nsTimer invalidate];
    
    [player pause];
    
    [nsTimer2 invalidate];
}
- (IBAction)touchUp:(id)sender {
    
    
    // called 3rd
    
    [player play];
    //[self.audioSliderBar setMaximumValue:2.46];
    
//    [self.audioSliderBar setMaximumValue: 1.0f];
//    [self.audioSliderBar setMinimumValue:0.0f];
    
    nsTimer2=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime2:) userInfo:nil repeats:YES];
    
}



@end


